import nodemailer from 'nodemailer';
import hbs from 'nodemailer-express-handlebars';
import Logger from './Logger';
import mailconfig from '../config/mail';
import { SendMailParams } from '../@types/common';

class Mail {
  private _transporter;

  constructor() {
    const hbsConfig = mailconfig.mailHbsOptions;
    this._transporter = nodemailer.createTransport(mailconfig.transportOption);
    this._transporter.use('compile', hbs(hbsConfig));
  }

  send = (options: SendMailParams) => {
    if (typeof options.from === 'undefined') {
      options.from = mailconfig.from;
    }

    this._transporter.sendMail(options, (error, info) => {
      error
        ? Logger.error(error)
        : Logger.info('Message sent: ' + info.messageId);
    });
  };
}

export default new Mail();

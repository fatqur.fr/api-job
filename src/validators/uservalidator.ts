import { Request, Response, NextFunction } from 'express';
import { check, CustomValidator } from 'express-validator';
import validator from 'validator';
import multer from 'multer';
import config from '../config';
import ResponseController from '../controllers/ResponseController';
// import PasswordService from '../services/PasswordService';
import UserService from '../services/UserService';
import { singleImageUpload } from '../utils/Upload';

const avatarValidation = (req: Request, res: Response, next: NextFunction) => {
  const maxsize = 1000;
  const avatar = singleImageUpload('avatar', 'avatar', maxsize);

  avatar(req, res, (err) => {
    if (err instanceof multer.MulterError) {
      let message = '';
      if (err.code === 'LIMIT_FILE_SIZE') {
        message = req.t('upload.max_size', { maxsize: `${maxsize / 1000}MB` });
      } else if (err.field !== 'avatar') {
        message = req.t('upload.unhandle', { filename: err.field });
      } else {
        message = req.t('upload.image');
      }
      return ResponseController.unprocessableEntity(res, {
        errors: [
          {
            msg: message,
            param: 'avatar',
            location: 'body',
          },
        ],
      });
    } else {
      next();
    }
  });
};

const isDate: CustomValidator = async (input, { req, path }) => {
  if (!validator.isDate(input)) {
    return Promise.reject(req.t('validator.date', { field: path }));
  }
};

// for profile
const requireOccupation: CustomValidator = async (input, { req, path }) => {
  if (req.app.locals.user.usertype === 'patient') {
    if (validator.isEmpty(input)) {
      return Promise.reject(req.t('validator.required', { field: path }));
    }
  }
};

// for profile
const requireContactType: CustomValidator = async (input, { req, path }) => {
  if (req.app.locals.user.usertype === 'patient') {
    if (validator.isEmpty(input)) {
      return Promise.reject(req.t('validator.required', { field: path }));
    }
  }
};

// for change email
const differentEmail: CustomValidator = async (input, { req }) => {
  if (input === req.app.locals.user.email) {
    return Promise.reject(req.t('auth.email_same'));
  }
};

// const emailNotExist: CustomValidator = async (input, { req }) => {
//   let isExist = null;
//   if (req.params?.officerId) {
//     const email = await OfficerService.getEmail(
//       parseInt(req.params.officerId, 10)
//     );
//     if (email !== input) {
//       isExist = await UserService.isExist(input);
//     }
//   } else if (req.params?.medicalId) {
//     const email = await PatientService.getEmail(req.params.medicalId);
//     if (email !== input) {
//       isExist = await UserService.isExist(input);
//     }
//   } else {
//     isExist = await UserService.isExist(input);
//   }
//   if (isExist) {
//     return Promise.reject(req.t('auth.email_exist', { value: input }));
//   }
// };

// const currentPassword: CustomValidator = async (input, { req }) => {
//   const valid = await PasswordService.checkPassword(
//     req.app.locals.user.id,
//     input
//   );
//   if (!valid) {
//     return Promise.reject(req.t('password.incorrect'));
//   }
// };

const language: CustomValidator = async (input, { req, path }) => {
  const lang = ['en', 'id'];
  if (!lang.includes(input)) {
    return Promise.reject(
      req.t('validator.whichone', { field: path, item1: 'en', item2: 'id' })
    );
  }
};

const nationality: CustomValidator = async (input, { req, path }) => {
  const lang = ['WNI', 'WNA'];
  if (!lang.includes(input)) {
    return Promise.reject(
      req.t('validator.whichone', { field: path, item1: 'en', item2: 'id' })
    );
  }
};

const isnumeric: CustomValidator = async (input, { req, path }) => {
  if (!validator.isNumeric(input)) {
    return Promise.reject('Medical ID must numeric');
  }
};

// Modul: Profile
const updateProfileValidation = [
  check('firstName')
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    })
    .bail()
    .isString()
    .trim(),

  check('lastName')
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    })
    .bail()
    .isString(),
];

const changeLanguageValidator = [
  check('lang')
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    })
    .bail()
    .custom(language),
];

const changeEmailValidation = [
  check('email')
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    })
    .bail()
    .normalizeEmail()
    .isEmail()
    .withMessage((value, { req, path }) => {
      return req.t('validator.email', { field: path });
    })
    .custom(differentEmail)
    .bail(),
  // .custom(emailNotExist),

  check('password')
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    })
    .bail(),
  // .custom(currentPassword),
];

export {
  // Module: profile
  avatarValidation,
  updateProfileValidation,
  changeLanguageValidator,
  changeEmailValidation,
};

import { check, CustomValidator } from 'express-validator';
import config from '../config';
import RoleService from '../services/RoleService';
import { convertNameToCode } from '../utils/General';

const roleNotExist: CustomValidator = async (value, { req }) => {
  let isExist = null;
  if (req.params?.code) {
    if (req.params.code !== convertNameToCode(value)) {
      isExist = await RoleService.isExistRole(convertNameToCode(value));
    }
  } else {
    isExist = await RoleService.isExistRole(convertNameToCode(value));
  }
  if (isExist) {
    return Promise.reject(req.t('validator.exist', { field: 'role', value }));
  }
};

const roleCanBeUpdated: CustomValidator = async (value, { req }) => {
  if (req.params?.code) {
    if (req.params.code !== convertNameToCode(value)) {
      if (config.role.immutable.includes(req.params.code)) {
        return Promise.reject(
          req.t('validator.update_immutable_role', { value })
        );
      }
    }
  }
};

const roleExist: CustomValidator = async (value: string, { req }) => {
  const roleExist = await RoleService.isExistRole(value);
  if (!roleExist) {
    return Promise.reject(
      req.t('validator.not_exist', {
        field: 'Role',
        value: value,
      })
    );
  }
};

const roleCanBeDeleted: CustomValidator = async (value, { req }) => {
  if (config.role.immutable.includes(value)) {
    return Promise.reject(req.t('validator.delete_immutable_role', { value }));
  }
};

const isUsedRole: CustomValidator = async (value, { req }) => {
  if (await RoleService.isUsedRole(value)) {
    return Promise.reject(req.t('validator.used_role', { value }));
  }
};

const permissionsExist: CustomValidator = async (value: string[], { req }) => {
  let permsNotExist: string[] = [];
  await Promise.all(
    value.map(async (perm) => {
      if (!(await RoleService.isExistPermission(perm))) {
        permsNotExist.push(perm);
      }
    })
  ).then(() => {
    if (permsNotExist.length) {
      return Promise.reject(
        req.t('validator.not_exist', {
          field: 'Permissions',
          value: permsNotExist.join(', '),
        })
      );
    }
  });
};

const permissionNotExist: CustomValidator = async (value: string, { req }) => {
  const permsExist = await RoleService.isExistPermission(value);
  if (permsExist) {
    return Promise.reject(
      req.t('validator.exist', {
        field: 'Permissions',
        value: value,
      })
    );
  }
};

const permissionCodeExist: CustomValidator = async (value: string, { req }) => {
  const permsExist = await RoleService.isExistPermission(value);
  if (!permsExist) {
    return Promise.reject(
      req.t('validator.not_exist', {
        field: 'Permissions',
        value: value,
      })
    );
  }
};

const permissionCanBeDeleted: CustomValidator = async (value: string, { req }) => {
  const cannotDelete = ['create-roles', 'read-roles', 'update-roles', 'delete-roles'];
  if (cannotDelete.includes(value)) {
    return Promise.reject('cannot delete permission for roles module');
  }
};

const createRoleValidation = [
  check('name')
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    })
    .bail()
    .isString()
    .trim()
    .custom(roleNotExist),

  check('permissions')
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    })
    .bail()
    .isArray({ min: 1 })
    .withMessage((value, { req, path }) => {
      return req.t('validator.greater_than', { field: path, count: 1 });
    })
    .bail()
    .custom(permissionsExist),
];

const updateRoleValidation = [
  check('name')
    .optional({ nullable: true, checkFalsy: true })
    .isString()
    .bail()
    .trim()
    .bail()
    // .custom(roleCanBeUpdated)
    // .bail()
    .custom(roleNotExist),

  check('permissions')
    .notEmpty()
    .withMessage((value, { req, path }) => {
      console.log(req.params?.code ? 'ada' : 'enggak');
      return req.t('validator.required', { field: path });
    })
    .bail()
    .isArray({ min: 1 })
    .withMessage((value, { req, path }) => {
      return req.t('validator.greater_than', { field: path, count: 1 });
    })
    .bail()
    .custom(permissionsExist),
];

const deleteRoleValidation = [
  check('code')
    .custom(roleExist)
    .bail()
    .custom(roleCanBeDeleted)
    .bail()
    .custom(isUsedRole),
];

// -- Permission: for development purposes only --
const createPermissionValidation = [
  check('code')
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    })
    .bail()
    .isString()
    .trim()
    .custom(permissionNotExist),

  check('module')
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    })
    .bail()
    .isString()
    .trim(),

  check('action')
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    })
    .bail()
    .isString()
    .trim(),
];
const deletePermissionValidation = [
  check('code')
    .custom(permissionCodeExist)
    .bail()
    .custom(permissionCanBeDeleted),
];
// -- End Permission --

export {
  createRoleValidation,
  updateRoleValidation,
  deleteRoleValidation,
  createPermissionValidation,
  deletePermissionValidation
};

import EventEmitter from 'events';
import ChangedEmailNotification from '../notifications/EmailChangeNotification';
import NewUserNotification from '../notifications/NewUserNotification';
import PasswordChangedNotification from '../notifications/PasswordChangedNotification';
import UserService from '../services/UserService';
import Logger from '../utils/Logger';

const userEvent = new EventEmitter();

// userEvent.on('created', (email: string) => {
//   setImmediate(async () => {
//     try {
//       const token = await UserService.getVerifyToken(email);
//       if (token) {
//         new NewUserNotification(email, token);
//       }
//     } catch (error: any) {
//       Logger.error(error);
//     }
//   });
// });

// userEvent.on('changeEmail', (userId: number, newemail: string) => {
//   setImmediate(async () => {
//     try {
//       const token = await UserService.generateTokenEmail(userId, newemail);
//       new ChangedEmailNotification(newemail, token);
//     } catch (error: any) {
//       Logger.error(error);
//     }
//   });
// });

userEvent.on('passwordChanged', (email: string) => {
  setImmediate(async () => {
    try {
      new PasswordChangedNotification(email);
    } catch (error: any) {
      Logger.error(error);
    }
  });
});

export default userEvent;

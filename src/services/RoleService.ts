import { Permission, Prisma } from '@prisma/client';
import config from '../config';
import { PaginationRequest, PaginationResult } from '../@types/pagination';
import { roleReqBody } from '../@types/role';
import { convertNameToCode } from '../utils/General';
import Pagination from '../utils/Pagination';
import { prisma } from '../utils/PrismaClient';

class RoleService {
  private static formatPermissions(permissions: Permission[]) {
    let tempData: any = {};
    permissions.forEach((permission) => {
      let module = permission.module;
      if (!tempData[module]) {
        tempData[module] = [];
      }
      tempData[module].push({
        module: permission.module,
        code: permission.code,
        name: permission.name,
      });
    });

    const result = [];
    for (let item in tempData) {
      result.push({
        module: item,
        data: tempData[item],
      });
    }
    return result;
  }

  public static async getFormattedPermissions() {
    let data: any = null;
    const permissions = await prisma.permission.findMany();
    if (permissions) {
      data = this.formatPermissions(permissions);
    }
    return data;
  }

  public static async getRoles(
    params: PaginationRequest
  ): Promise<PaginationResult> {
    let where: Prisma.RoleWhereInput | undefined = params.search
        ? {
            OR: [
              {
                code: {
                  contains: params.search.toLowerCase(),
                }
              },
              {
                name: {
                  contains: params.search.toLowerCase(),
                }
              }
            ]
          }
        : undefined;

    const limit = params.size ? parseInt(params.size) : config.pagination.size;
    const offset = params.page ? (parseInt(params.page) - 1) * limit : 0;

    const orderDir = params.orderDir || 'asc';
    let order = {};
    switch (params.orderBy) {
      case 'code':
        order = { code: orderDir };
        break;
      case 'name':
        order = { name: orderDir };
        break;
      case 'permissions':
        order = { permissions: { _count: orderDir } };
        break;
      case 'users':
        order = { users: { _count: orderDir } };
        break;
      case 'created':
        order = { createdAt: orderDir };
        break;
      default:
        order = { name: orderDir };
        break;
    }

    const [total, roles] = await Promise.all([
      prisma.role.count({ where: where }),
      prisma.role.findMany({
        include: {
          _count: true,
        },
        where: where,
        orderBy: order,
        skip: offset,
        take: limit,
      }),
    ]);
    return Pagination.formatResult(params, roles, total);
  }

  public static async getRoleList() {
    return await prisma.role.findMany({
      select: { code: true, name: true },
      where: { code: { notIn: [config.role.superadmin] } },
      orderBy: { name: 'asc' },
    });
  }

  public static async create(params: roleReqBody) {
    let connect_perms: { code: string }[] = [];
    params.permissions.forEach((perms) => {
      connect_perms.push({ code: perms });
    });

    const role = await prisma.role.create({
      data: {
        name: params.name,
        code: convertNameToCode(params.name),
        permissions: {
          connect: connect_perms,
        },
      },
      include: {
        permissions: {
          select: {
            code: true,
          },
        },
      },
    });

    return role;
  }

  public static async show(code: string, permissions_code: boolean = false) {
    const role = await prisma.role.findUnique({
      where: { code },
      include: {
        permissions: true,
      },
    });

    if (role) {
      let formattedPermissions: any[] = [];
      if (permissions_code) {
        role.permissions.forEach((permission) => {
          formattedPermissions.push(permission.code);
        });
      } else {
        formattedPermissions = this.formatPermissions(role.permissions);
      }

      return {
        code: role.code,
        name: role.name,
        permissions: formattedPermissions,
      };
    } else {
      return null;
    }
  }

  public static async update(code: string, params: roleReqBody) {
    let connect_perms: { code: string }[] = [];
    params.permissions.forEach((perms) => {
      connect_perms.push({ code: perms });
    });
    const updatedRole = await prisma.role.update({
      where: { code },
      data: {
        name: params.name,
        // code: convertNameToCode(params.name),
        permissions: { set: connect_perms },
      },
      include: {
        permissions: {
          select: {
            code: true,
          },
        },
      },
    });
    return updatedRole;
  }

  public static async delete(code: string) {
    return await prisma.role.delete({ where: { code } });
  }

  public static async isExistPermission(code: string): Promise<boolean> {
    const exist = await prisma.permission.findFirst({ where: { code } });
    return exist ? true : false;
  }

  public static async isExistRole(code: string): Promise<boolean> {
    const exist = await prisma.role.findFirst({ where: { code } });
    return exist ? true : false;
  }

  public static async isUsedRole(code: string): Promise<boolean> {
    const role = await prisma.role.findFirst({
      where: { code },
      select: {
        _count: { select: { users: true } },
      },
    });

    return role?._count.users ? true : false;
  }

  public static async getUserInRole(code: string) {
    const role = await prisma.role.findUnique({
      where: { code },
      select: {
        users: {
          select: { id: true, email: true, name: true },
        },
      },
    });
    return role?.users;
  }

  // for development purposes only
  public static async createPermission(data: {
    code: string;
    module: string;
    name: string;
  }) {
    const permission = await prisma.permission.create({ data });
    return permission;
  }

  public static async deletePermission(code: string) {
    return await prisma.permission.delete({ where: { code } });
  }
}
export default RoleService;

import { User } from '@prisma/client';
import config from '../config';
import { prisma } from '../utils/PrismaClient';
import {
  emergencyContactParams,
  profileUpdate,
  userCreateParams,
} from '../@types/user';

class UserService {
  public static async isExist(email: string): Promise<boolean> {
    const exist = await prisma.user.findFirst({ where: { email } });
    return exist ? true : false;
  }

  public static async find(id: number) {
    return await prisma.user.findUnique({
      where: { id },
      include: { roles: true },
    });
  }

  public static async findUserByEmail(
    email: string,
    status?: any
  ): Promise<User | null> {
    let userStatus = status ? status : undefined;
    const user = await prisma.user.findFirst({
      where: { email, status: userStatus },
    });
    return user;
  }

  public static async create(data: userCreateParams) {
    const user = await prisma.user.create({
      data: {
        name: data.name,
        email: data.email,
        password: data.password,
      },
    });
    return user;
  }

  // public static async findToken(email: string, token: string) {
  //   const result = await prisma.userVerification.findFirst({
  //     where: { token, user: { email } },
  //   });
  //   return result;
  // }

  // public static async getVerifyToken(email: string) {
  //   const user = await prisma.user.findUnique({ where: { email } });
  //   if (user) {
  //     const data = {
  //       key: random(16),
  //       expired: config.expiration.verify_account,
  //     };

  //     const token = Crypt.encrypt(JSON.stringify(data));

  //     await prisma.$transaction([
  //       prisma.userVerification.upsert({
  //         create: { token, userId: user.id },
  //         update: { token, createdAt: new Date() },
  //         where: { userId: user.id },
  //       }),

  //       prisma.passwordReset.upsert({
  //         create: { email, token },
  //         update: { token, createdAt: new Date() },
  //         where: { email },
  //       }),
  //     ]);

  //     return token;
  //   } else {
  //     return null;
  //   }
  // }

  public static async deactivate(email: string) {
    const user = await prisma.user.update({
      where: { email },
      data: {
        status: 'inactive',
      },
    });
    return user;
  }

  public static async activate(email: string) {
    const updated = await prisma.user.update({
      where: { email },
      data: {
        status: 'active',
      },
    });
    return updated;
  }

  public static async profile(id: number) {
    let user = await prisma.user.findUnique({
      where: { id },
      select: {
        name: true,
        lang: true,
        avatar: true,
      },
    });

    return user;
  }

  public static async updateProfile(
    id: number,
    data: profileUpdate,
    contact: emergencyContactParams
  ) {
    const updateUser = await prisma.user.update({
      data: {
        name: data.name,
        avatar: data.avatar,
      },
      where: { id },
    });
    let user = await prisma.user.findUnique({
      where: { id },
      select: {
        name: true,
        lang: true,
        avatar: true,
      },
    });
    return user;

    return updateUser;
  }

  public static async changeLang(id: number, lang: string) {
    const updateUser = await prisma.user.update({
      data: {
        lang: lang as any,
      },
      where: { id },
    });
    const user = await prisma.user.findUnique({
      where: { id },
      select: { name: true, lang: true },
    });
    return user;
  }

  public static async getAvatar(id: number) {
    const user = await prisma.user.findFirst({
      where: { id },
      select: { avatar: true },
    });
    return user?.avatar;
  }

  // public static async generateTokenEmail(
  //   userId: number,
  //   newEmail: string
  // ): Promise<string> {
  //   const data = {
  //     key: random(16),
  //     expired: config.expiration.change_email,
  //   };

  //   const token = Crypt.encrypt(JSON.stringify(data));
  //   await prisma.changeEmail.upsert({
  //     create: { user: { connect: { id: userId } }, token, email: newEmail },
  //     update: { email: newEmail, token, createdAt: new Date() },
  //     where: { userId },
  //   });

  //   return token;
  // }

  // public static async findTokenEmail(email: string, token: string) {
  //   const result = await prisma.changeEmail.findFirst({
  //     where: { token, email },
  //   });
  //   return result;
  // }

  // public static async findUserChangeEmail(email: string, token: string) {
  //   const result = await prisma.changeEmail.findFirst({
  //     select: {
  //       user: true,
  //     },
  //     where: { token, email },
  //   });
  //   return result?.user;
  // }

  public static async changeEmail(id: number, email: string) {
    const userUpdate = await prisma.user.update({
      where: { id },
      data: { email },
    });

    return userUpdate;
  }

  // public static async deletTokenEmail(email: string) {
  //   const deleted = await prisma.changeEmail.delete({ where: { email } });
  //   return deleted;
  // }

  // public static async lastActivity(id: number) {
  //   const user = await prisma.user.update({
  //     where: { id },
  //     data: { lastActivityAt: new Date() },
  //   });
  //   return user;
  // }

  // public static async userSetInactiveLast(days: number) {
  //   const user = await prisma.user.updateMany({
  //     data: { status: 'inactive' },
  //     where: {
  //       status: 'active',
  //       lastActivityAt: {
  //         lt: new Date(new Date().setDate(new Date().getDate() - days)),
  //       },
  //     },
  //   });
  //   return user;
  // }

  public static async patientSetInactiveLast(days: number) {
    const users: { id: number; userId: number; medicalId: string }[] =
      await prisma.$queryRawUnsafe(
        `select p.id, p."userId" , p."medicalId", reg.regdate, p."createdAt"  from public."Patient" p 
      left join (select r."patientId", max(r."date") as regdate from public."Registration" r group by r."patientId") 
      reg on p.id = reg."patientId"
      where case when regdate is null then DATE(p."createdAt") < current_date - interval '${days}' day
        else DATE(regdate) < current_date - interval '${days}' day
      end`
      );

    for (const user of users) {
      const update = await prisma.user.update({
        data: { status: 'inactive' },
        where: { id: user.userId },
      });
    }

    return users;
  }

  public static async setFCMToken(id: number, token: string | null) {
    const user = await prisma.user.update({
      where: { id },
      data: { fcmToken: token },
    });
    return user;
  }
}

export default UserService;

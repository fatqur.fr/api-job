import axios from 'axios';
import config from '../config';
import Pagination from '../utils/Pagination';
import { PaginationRequest, PaginationResult } from '../@types/pagination';
import { prisma } from '../utils/PrismaClient';
import { rawposition } from '../@types/position';
import { Prisma } from '@prisma/client';

class PositionService {
  public static async initData() {
    const { data } = await axios.get(
      'http://dev3.dansmultipro.co.id/api/recruitment/positions.json'
    );
    for (const item of data as rawposition[]) {
      const upsert = await prisma.position.upsert({
        where: {
          id: item.id,
        },
        update: {
          type: item.type,
          url: item.url,
          company: item.company,
          company_url: item.company_url,
          location: item.location,
          title: item.title,
          description: item.description,
          how_to_apply: item.how_to_apply,
          company_logo: item.company_logo,
          created_at: new Date(item.created_at),
        },
        create: {
          id: item.id,
          type: item.type,
          url: item.url,
          company: item.company,
          company_url: item.company_url,
          location: item.location,
          title: item.title,
          description: item.description,
          how_to_apply: item.how_to_apply,
          company_logo: item.company_logo,
          created_at: new Date(item.created_at),
        },
      });
    }
  }

  public static async getAll(
    params: PaginationRequest
  ): Promise<PaginationResult> {
    let where: Prisma.PositionWhereInput | undefined = params.search
      ? {
          OR: [
            {
              description: {
                contains: params.search.toLowerCase(),
              },
            },
            {
              location: {
                contains: params.search.toLowerCase(),
              },
            },
            {
              type: {
                contains: params.search.toLowerCase(),
              },
            },
          ],
        }
      : undefined;

    const limit = params.size ? parseInt(params.size) : config.pagination.size;
    const offset = params.page ? (parseInt(params.page) - 1) * limit : 0;

    const orderDir = params.orderDir || 'asc';
    let order = {};
    switch (params.orderBy) {
      case 'type':
        order = { type: orderDir };
        break;
      case 'created_at':
        order = { created_at: orderDir };
        break;
      case 'company':
        order = { company: orderDir };
        break;
      case 'location':
        order = { location: orderDir };
        break;
      case 'title':
        order = { title: orderDir };
        break;
      default:
        order = { created_at: 'desc' };
        break;
    }

    const [total, positions] = await Promise.all([
      prisma.position.count({ where: where }),
      prisma.position.findMany({
        where: where,
        orderBy: order,
        skip: limit > 0 ? offset : undefined,
        take: limit > 0 ? limit : undefined,
      }),
    ]);
    return Pagination.formatResult(params, positions, total);
  }

  public static async show(id: string) {
    const position = await prisma.position.findUnique({ where: { id } });
    return position;
  }
}
export default PositionService;

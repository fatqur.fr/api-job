import { IRateLimiterOptions } from 'rate-limiter-flexible';

export const loginOptions: IRateLimiterOptions = {
  points: 5,
  duration: 30 * 60, //30 minutes
};

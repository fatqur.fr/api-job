import { InitOptions } from 'i18next';
import path from 'path';

const i18Options: InitOptions = {
  fallbackLng: 'en',
  preload: ['en', 'id'],
  ns: ['translation'],
  defaultNS: 'translation',
  backend: {
    loadPath: path.join(
      __dirname,
      '..',
      '..',
      'locales',
      '{{lng}}',
      '{{ns}}.json'
    ),
  },
  returnNull: false,
};
export default i18Options;

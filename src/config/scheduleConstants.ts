/**
 * Cron schedule expressions
 * @see https://crontab.guru/
 * 
 */
export const EVERY_MINUTE = '* * * * *';
export const EVERY_30_MINUTES = '*/30 * * * *';
export const EVERY_HOUR = '0 * * * *';
export const EVERY_MIDNIGHT = '0 0 * * *';
export const EVERY_8_AM = '0 8 * * *';

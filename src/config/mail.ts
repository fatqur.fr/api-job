import dotenv from 'dotenv';
import path from 'path';

dotenv.config();
export default {
  from: process.env.MAIL_FROM || 'Fire Express <noreply@fire-express.com>',
  transportOption: {
    host: process.env.MAIL_HOST || 'smtp.mailtrap.io',
    port: Number(process.env.MAIL_PORT) || 2525,
    auth: {
      user: process.env.MAIL_USERNAME || 'b6b595c409f459',
      pass: process.env.MAIL_PASSWORD || '3b8005e1b981ae',
    },
  },
  mailHbsOptions: {
    viewEngine: {
      extname: '.hbs',
      layoutsDir: path.join(__dirname, '..', 'views', 'emails', 'layouts'),
      partialsDir: path.join(__dirname, '..', 'views', 'emails', 'partials'),
      defaultLayout: 'index',
    },
    viewPath: path.join(__dirname, '..', 'views', 'emails'),
    extName: '.hbs',
  },
};

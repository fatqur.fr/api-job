import AuthRoutes from './AuthRoutes';
import BaseRoutes from './BaseRoutes';
import PositionRoutes from './PositionRoutes';
// import ProfileRoutes from './ProfileRoutes';
// import RoleRoutes from './RoleRoutes';

class AppRoute extends BaseRoutes {
  routes(): void {
    //application routes
    this.router.use('/auth', AuthRoutes);
    this.router.use('/jobs', PositionRoutes);
    // this.router.use('/profile', ProfileRoutes);
    // this.router.use('/roles', RoleRoutes);
  }
}
export default new AppRoute().getRouter();

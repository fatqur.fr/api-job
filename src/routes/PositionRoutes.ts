import PositionController from '../controllers/PositionController';
import auth from '../middleware/auth';
import can from '../middleware/authorize';
import BaseRoutes from './BaseRoutes';

class PositionRoutes extends BaseRoutes {
  routes(): void {
    this.router.post(
      '/init',
      auth,
      can('add_position'),
      PositionController.init
    );

    this.router.get(
      '/',
      auth,
      can('browse_position'),
      PositionController.index
    );
    this.router.get(
      '/:id',
      auth,
      can('read_position'),
      PositionController.show
    );
  }
}
export default new PositionRoutes().getRouter();

import { Router } from 'express';

abstract class BaseRoutes {
  protected router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  public getRouter() {
    return this.router;
  }

  abstract routes(): void;
}

export default BaseRoutes;

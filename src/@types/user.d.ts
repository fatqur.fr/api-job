interface user {
  name: string;
  email: string;
}

interface userUpdate {
  name: string;
  role?: string;
  email?: string;
}

interface userDetail {
  placeOfBirth: string;
  dateOfBirth: string;
  gender: string;

  nationality: string;

  religionCode: string;
  religionNote?: string;

  identityTypeCode: string;
  identityTypeNote?: string;
  identityNumber: string;

  educationCode: string;
  educationNote?: string;

  maritalStatusCode: string;
  maritalStatusNote?: string;

  address: string;

  housePhone?: string;
  mobilePhone?: string;
  officePhone?: string;
}

interface officerEmergencyContactParams {
  contactName: string;
  contactAddress: string;

  contactIdentityTypeCode: string;
  contactIdentityTypeNote?: string;
  contactIdentityNumber: string;

  contactHousePhone?: string;
  contactMobilePhone?: string;
  contactOfficePhone?: string;
}

export interface userCreateParams extends user {
  password: string;
  type: string;
  status?: string;
  lang?: string;
  role?: string;
}

export interface userUpdateParams extends userUpdate {
  status?: string;
  lang?: string;
}

export interface emergencyContactParams extends officerEmergencyContactParams {
  contactType: string;
}

export interface patientCreateParams extends userDetail {
  medicalId?: string;

  occupationCode: string;
  occupationNote?: string;

  haveChild?: string;
  patientType: string;
  doctorId?: string;
  assignedBy?: number;

  rt: string;
  rw: string;
  kelurahan: string;
  kecamatan: string;
  kota: string;
  provinsi: string;
  country: string;
  zipcode: string;
}

export interface patientUpdateParams extends userDetail {
  occupationCode: string;
  occupationNote?: string;

  haveChild?: string;
  patientType: string;

  doctorId?: string;
  assignedBy?: number;

  rt: string;
  rw: string;
  kelurahan: string;
  kecamatan: string;
  kota: string;
  provinsi: string;
  country: string;
  zipcode: string;
}

export interface officerParams extends userDetail {
  specialistCode?: string;
  npwp?: string;
  isBS3?: string;
  isBS5?: string;
}

export type patientCreateRequest = patientCreateParams &
  user &
  emergencyContactParams;
export type patientUpdateRequest = patientUpdateParams &
  userUpdate &
  emergencyContactParams;

export type officerCreateRequest = officerParams &
  userCreateParams &
  officerEmergencyContactParams;
export type officerUpdateRequest = officerParams &
  userUpdate &
  officerEmergencyContactParams;

export interface profileUpdate extends userDetail {
  name: string;
  type?: string;
  avatar?: string;

  occupationCode?: string;
  occupationNote?: string;
  haveChild?: string;

  specialistCode?: string;
}

export interface patientHis {
  MR_No: string;
  Patient_Name: string;
  Nick_Name?: string;
  Place_Of_Birth?: string;
  dob: string;
  Gender?: string;
  Home_Address?: string;
  RtRw?: string;
  KTP_RW?: string;
  Kelurahan?: string;
  Kecamatan?: string;
  Wilayah_Kota?: string;
  KTP_Propinsi?: string;
  State?: string;
  KTP_Zip_Code?: string;
  Nationality?: string;
  No_Ktp?: string;
  Education?: string;
  Pekerjaan?: string;
  Status_Perkawinan?: string;
  Telepon?: string;
  HP?: string;
  Email?: string;
}

export interface doctorHIS {
  BS3: string;
  BS5: string;
  Doctor_Name: string;
  Doctor_Nick_Name: string;
  DOB: string;
  Home_Address: string;
  Gender: string;
  Religion: string;
  Status_Martial: string;
  Active: string;
  NPWP: string;
}

export interface dataDoctorHIS {
  Action: string;
  Branch: string;
  Doctor_Code: string;
  Doctor_Name: string;
  Doctor_Display: string;
  Doctor_Nick_Name: string;
  DOB: string;
  Gender: string;
  Religion: string;
  Home_Address: string;
  Marital_Status: string;
  NPWP: string;
  Active: string;
  User_entry: string;
  No_KTP: string;
}

export interface SendMailParams {
  from?: string;
  to: string;
  subject: string;
  template: string;
  context: any;
}

export type dbnotification = {
  module: string;
  userId: number;
  titleEn: string;
  titleId: string;
  messageEn?: string;
  messageId?: string;
  data?: string;
  readAt?: string;
};

export type sendFCMParam = {
  userId: number;
  notifId: number;
  title: string;
  message: string;
};

import { Response } from 'express';

class ResponseController {
  public static unauthorized(res: Response, message?: string): Response {
    return res
      .status(401)
      .json({ status: false, message: message ?? 'Unauthorized' });
  }

  public static forbidden(res: Response, message?: string): Response {
    return res
      .status(403)
      .json({ status: false, message: message ?? 'Forbidden' });
  }

  public static tooMany(res: Response, retyAfter?: number): Response {
    type responseData = {
      status: boolean;
      message: string;
      retyAfter?: number;
    };

    let data: responseData = {
      status: false,
      message: 'Too many requests',
    };
    if (retyAfter) {
      data.retyAfter = retyAfter;
    }
    return res.status(429).json(data);
  }

  public static unprocessableEntity(res: Response, errorResult: any, message?: string): Response {
    return res.status(422).json({
      status: false,
      message: message ?? 'The given data was invalid',
      errors: errorResult.errors,
    });
  }
}
export default ResponseController;

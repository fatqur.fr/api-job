import { Request, Response } from 'express';
import BaseController from './BaseController';
import config from '../config';
import RoleService from '../services/RoleService';
import { PaginationRequest } from '../@types/pagination';
import { roleReqBody, roleReqParams } from '../@types/role';
import { getPathUrl } from '../utils/General';

class RoleController extends BaseController {
  public index = async (req: Request, res: Response) => {
    try {
      const params = req.query as PaginationRequest;
      params.path = getPathUrl(req);
      const roles = await RoleService.getRoles(params);
      // this.lastActivity(req.app.locals.user.id);

      return this.success(
        res,
        req.t('response.getdata', {
          item: req.language === 'id' ? 'peran' : 'roles',
        }),
        roles
      );
    } catch (error: any) {
      return this.error(res, error, req.t('response.error'));
    }
  };

  public list = async (req: Request, res: Response) => {
    try {
      const roles = await RoleService.getRoleList();
      return this.success(
        res,
        req.t('response.getdata', {
          item: req.language === 'id' ? 'peran' : 'roles',
        }),
        roles
      );
    } catch (error: any) {
      return this.error(res, error, req.t('response.error'));
    }
  };

  public create = async (req: Request, res: Response) => {
    try {
      const params = req.body as roleReqBody;
      const role = await RoleService.create(params);

      return this.created(
        res,
        req.t('response.created', {
          item: req.language === 'id' ? 'peran' : 'roles',
        }),
        { role }
      );
    } catch (error: any) {
      return this.error(res, error, req.t('response.error'));
    }
  };

  public show = async (req: Request, res: Response) => {
    try {
      const { code } = req.params as roleReqParams;
      const { permission_code } = req.query as { permission_code?: string };
      const using_permission_code =
        permission_code === 'true' || permission_code === '1';
      const role = await RoleService.show(code, using_permission_code);
      // this.lastActivity(req.app.locals.user.id);
      return role
        ? this.success(
            res,
            req.t('response.getdata', {
              item: req.language === 'id' ? 'peran' : 'role',
            }),
            { role }
          )
        : this.notFound(res, req.t('response.notfound'));
    } catch (error: any) {
      return this.error(res, error, req.t('response.error'));
    }
  };

  public update = async (req: Request, res: Response) => {
    try {
      const { code } = req.params as roleReqParams;
      const params = req.body as roleReqBody;
      const oldrole = await RoleService.show(code, true);
      if (code === config.role.superadmin) {
        if (!params.permissions.includes('create-roles')) {
          params.permissions.push('create-roles');
        }
        if (!params.permissions.includes('read-roles')) {
          params.permissions.push('read-roles');
        }
        if (!params.permissions.includes('update-roles')) {
          params.permissions.push('update-roles');
        }
        if (!params.permissions.includes('delete-roles')) {
          params.permissions.push('delete-roles');
        }
      }
      const role = await RoleService.update(code, params);

      return this.success(
        res,
        req.t('response.updated', {
          item: req.language === 'id' ? 'peran' : 'role',
        }),
        { role }
      );
    } catch (error: any) {
      return this.error(res, error, req.t('response.error'));
    }
  };

  public delete = async (req: Request, res: Response) => {
    try {
      const { code } = req.params as roleReqParams;
      const deleted = await RoleService.delete(code);

      return this.success(
        res,
        req.t('response.deleted', {
          item: req.language === 'id' ? 'peran' : 'role',
        }),
        deleted
      );
    } catch (error: any) {
      return this.error(res, error, req.t('response.error'));
    }
  };

  public permissions = async (req: Request, res: Response) => {
    try {
      const permissions = await RoleService.getFormattedPermissions();
      // this.lastActivity(req.app.locals.user.id);

      return this.success(
        res,
        req.t('response.getdata', {
          item: req.language === 'id' ? 'izin' : 'permissions',
        }),
        { permissions }
      );
    } catch (error: any) {
      return this.error(res, error, req.t('response.error'));
    }
  };

  // for development purposes only
  public createPermission = async (req: Request, res: Response) => {
    try {
      const params = req.body as {
        code: string;
        module: string;
        name: string;
      };

      const permission = await RoleService.createPermission(params);

      return this.created(
        res,
        req.t('response.created', {
          item: req.language === 'id' ? 'izin' : 'permission',
        }),
        { permission }
      );
    } catch (error: any) {
      return this.error(res, error, req.t('response.error'));
    }
  };

  public deletePermission = async (req: Request, res: Response) => {
    try {
      const { code } = req.params as { code: string };
      const deleted = await RoleService.deletePermission(code);
      return this.success(
        res,
        req.t('response.deleted', {
          item: req.language === 'id' ? 'izin' : 'permission',
        }),
        deleted
      );
    } catch (error: any) {
      return this.error(res, error, req.t('response.error'));
    }
  };
}

export default new RoleController();

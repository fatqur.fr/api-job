import { Request, Response } from 'express';
import BaseController from './BaseController';
import { PaginationRequest } from '../@types/pagination';
import { getPathUrl } from '../utils/General';
import PositionService from '../services/PositionService';

class PositionController extends BaseController {
  public init = async (req: Request, res: Response) => {
    try {
      await PositionService.initData();
      return this.success(res, req.t('response.load'), null);
    } catch (error: any) {
      return this.error(res, error, req.t('response.error'));
    }
  };

  public index = async (req: Request, res: Response) => {
    try {
      const params = req.query as PaginationRequest;
      params.path = getPathUrl(req);
      const positions = await PositionService.getAll(params);

      return this.success(
        res,
        req.t('response.getdata', {
          item: req.language === 'id' ? 'posisi' : 'positions',
        }),
        positions
      );
    } catch (error: any) {
      return this.error(res, error, req.t('response.error'));
    }
  };

  public show = async (req: Request, res: Response) => {
    try {
      const { id } = req.params as { id: string };
      const position = await PositionService.show(id);
      return position
        ? this.success(
            res,
            req.t('response.getdata', {
              item: req.language === 'id' ? 'posisi' : 'position',
            }),
            position
          )
        : this.notFound(res, req.t('response.notfound'));
    } catch (error: any) {
      return this.error(res, error, req.t('response.error'));
    }
  };
}
export default new PositionController();

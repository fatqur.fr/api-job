import { Request, Response } from 'express';
import BaseController from './BaseController';
import url from '../config/url';
import userEvent from '../events/UserEvent';
import UserService from '../services/UserService';
import { emergencyContactParams, profileUpdate } from '../@types/user';
import { unlinkAvatar } from '../utils/Unlink';

class ProfileController extends BaseController {
  public index = async (req: Request, res: Response) => {
    try {
      const userId = req.app.locals.user.id;
      const user = await UserService.profile(userId);
      const item = req.language === 'id' ? 'profil' : 'profile';
      // this.lastActivity(userId);
      return this.success(res, req.t('response.getdata', { item }), user);
    } catch (error: any) {
      return this.error(res, error);
    }
  };

  public update = async (req: Request, res: Response) => {
    try {
      const userId = req.app.locals.user.id;
      let newAvatar = undefined;
      if (req.file) {
        newAvatar = `${url.base_url}/avatar/${req.file?.filename}`;
        const avatar = await UserService.getAvatar(userId);
        if (avatar) {
          unlinkAvatar(avatar.replace(`${url.base_url}/`, ''));
        }
      }
      const oldprofile = UserService.profile(userId);
      const params = req.body as profileUpdate;
      const contact = req.body as emergencyContactParams;
      contact.contactType =
        req.app.locals.user.usertype == 'patient'
          ? contact.contactType
          : 'family';
      params.type = req.app.locals.user.usertype;
      params.avatar = newAvatar;
      const profile = await UserService.updateProfile(userId, params, contact);

      return this.success(
        res,
        req.t('response.updated', {
          item: req.language === 'id' ? 'profil' : 'profile',
        }),
        profile
      );
    } catch (error: any) {
      return this.error(res, error);
    }
  };

  public changeLang = async (req: Request, res: Response) => {
    try {
      const { lang } = req.body as { lang: string };
      const userId = req.app.locals.user.id;
      const userLang = await UserService.changeLang(userId, lang);
      // this.lastActivity(userId);

      const message =
        req.language === 'id'
          ? 'Berhasil memperbarui bahasa pengguna'
          : 'Successfully updated user language';
      return this.success(res, message, userLang);
    } catch (error: any) {
      return this.error(res, error);
    }
  };

  public changeEmail = async (req: Request, res: Response) => {
    try {
      const { email } = req.body as { email: string; password: string };
      const userId = req.app.locals.user.id;

      userEvent.emit('changeEmail', userId, email);

      // this.lastActivity(userId);
      return this.success(res, req.t('auth.email.notified', { email }));
    } catch (error: any) {
      return this.error(res, error);
    }
  };
}

export default new ProfileController();

import BaseNotification from './BaseNotification';
import url from '../config/url';
import UserService from '../services/UserService';
import { SendMailParams } from '../@types/common';
import Mail from '../utils/Mail';

class PasswordChangedNotification extends BaseNotification {
  private _email: string;
  constructor(email: string) {
    super('mail');

    this._email = email;
    this.notify();
  }

  async toMail(): Promise<void> {
    const user = await UserService.findUserByEmail(this._email);
    const mailOptions: SendMailParams = {
      to: this._email,
      subject: user?.lang === 'id' ? 'Kata Sandi Diubah' : 'Password Changed',
      template: user?.lang === 'id' ? 'passwordchanged-id' : 'passwordchanged',
      context: {
        title: user?.lang === 'id' ? 'Kata Sandi Diubah' : 'Password Changed',
        asset_url: url.base_url,
        name: user?.name,
      },
    };
    Mail.send(mailOptions);
  }

  toDatabase(): void {}
  toFCM(): void {}
}

export default PasswordChangedNotification;

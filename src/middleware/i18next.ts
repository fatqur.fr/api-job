import i18next from 'i18next';
import FsBackend from 'i18next-fs-backend';
import i18nextMiddleware from 'i18next-http-middleware';
import i18Options from '../config/i18next';

i18next.use(i18nextMiddleware.LanguageDetector).use(FsBackend).init(i18Options);

export { i18next, i18nextMiddleware };

import { NextFunction, Request, Response } from 'express';
import { RateLimiterMemory } from 'rate-limiter-flexible';
import { loginOptions } from '../config/limitter';
import ResponseController from '../controllers/ResponseController';
import AuthService from '../services/AuthService';

const rateLimiter = new RateLimiterMemory(loginOptions);

const loginLimiter = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { email, password } = req.body as {
    email: string;
    password: string;
  };

  const attempt = await AuthService.fastCheckAttempt(email, password);
  const key = `fe_${req.ip}_${email}`;

  if (attempt) {
    rateLimiter.delete(key);
    next();
  } else {
    rateLimiter
      .consume(key)
      .then(() => {
        next();
      })
      .catch((rateLimiterRes) => {
        return ResponseController.tooMany(
          res,
          Math.floor(rateLimiterRes.msBeforeNext / 1000)
        );
      });
  }
};

export default loginLimiter;

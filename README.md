# Fire Express
Boilerplate for your API needs.

## Installation

Running for Development
```
copy .env.example to .env and fill database credential
npm install
npx prisma db push
npx prisma db seed
npm run dev
```

Running for Production
```
npm run build
npm run start
```

## Author and Contributors
- Mohammad Fatqur Rohman

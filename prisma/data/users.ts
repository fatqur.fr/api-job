export const users = [
  {
    name: 'Super User',
    email: 'superman@mail.com',
    password: 'password',
    roles: [{ code: 'superadmin' }],
  },
];
